﻿namespace Routing;

public sealed record Route
{
	public string Name { get; init; }
	public IEnumerable<Point> Points { get; set; } = new List<Point>();
}