﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Routing;

public static class RoutingExtensions
{
	public static bool IsWhiteListException(this Exception exception, [CallerMemberName] string methodName = "", [CallerLineNumber] int lineNumber = 0)
	{
		return exception switch
		{
			null => false,
			AggregateException aggregateException => aggregateException.InnerExceptions.All(innerException => IsExceptionOnWhitelist(innerException, methodName, lineNumber)),
			_ => IsExceptionOnWhitelist(exception, methodName, lineNumber)
		};
	}

	private static bool IsExceptionOnWhitelist(Exception exception, string methodName, int lineNumber)
	{
		foreach (Type type in WhiteListExceptions)
		{
			if (exception.GetType().IsSubclassOf(type) || exception.GetType() == type)
				return true;
		}

		Console.WriteLine($"Method: {methodName} threw exception {exception.GetType().Name} at line {lineNumber}. This exception is not on the white-list and so cannot be caught.\n{exception}");

		return false;
	}

	private static readonly List<Type> WhiteListExceptions = new()
	{
		typeof(NullReferenceException),
		typeof(ArgumentNullException),
		typeof(ArgumentOutOfRangeException),
		typeof(InvalidEnumArgumentException),
		typeof(ArgumentException),
		typeof(IndexOutOfRangeException),
		typeof(KeyNotFoundException),
		typeof(TimeoutException),
		typeof(NotImplementedException),
		typeof(InvalidCastException),
		typeof(FormatException)
	};
}