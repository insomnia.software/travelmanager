﻿using System.Diagnostics;
using System.Xml;
using Newtonsoft.Json;
using NodaTime.Text;

namespace Routing;

public static class Import
{
	public static IReadOnlyCollection<Route> LoadAllRoutes(string jsonFilePath, string? routesFilePath = null)
	{
		IReadOnlyCollection<Route> routes;

		Stopwatch sw = new();
		sw.Start();
		if (File.Exists(jsonFilePath))
		{
			//Console.WriteLine($"Routes file found at {routesPath}");
			routes = JsonConvert.DeserializeObject<IReadOnlyCollection<Route>>(File.ReadAllText(jsonFilePath)) ?? new List<Route>();
			//Console.WriteLine($"Loaded {routes.Count} routes from json in {sw.ElapsedMilliseconds}ms");
		}
		else if (routesFilePath is not null)
		{
			string? directory = Path.GetDirectoryName(jsonFilePath);
			if (directory is null)
				throw new ArgumentException($"Return of GetDirectoryName({jsonFilePath}) was null");
			Directory.CreateDirectory(directory);
			Console.WriteLine($"Did not find routes file found at {jsonFilePath}. Generating it...");
			routes = GetAllRoutes(routesFilePath);
			Console.WriteLine($"Loaded {routes.Count} routes in {sw.ElapsedMilliseconds}ms");

			File.WriteAllText(jsonFilePath, JsonConvert.SerializeObject(routes));
		}
		else
		{
			Console.WriteLine($"Failed to find json file at {jsonFilePath} and no route path was supplied");
			return new List<Route>();
		}

		return routes;
	}

	private static IReadOnlyCollection<Route> GetAllRoutes(string parentFolder)
	{
		List<Route> routes = new();
		foreach (string file in Directory.EnumerateFiles(parentFolder, "*.gpx", SearchOption.AllDirectories))
		{
			try
			{
				routes.Add(LoadRoute(file));
			}
			catch (Exception ex) when (ex.IsWhiteListException())
			{
				Console.WriteLine($"Route {file} threw exception {ex}");
			}
		}

		return routes;
	}

	private static Route LoadRoute(string filePath)
	{
		(Route route, XmlNode trk) = GetNodeAndTrack(filePath);

		if (trk.ChildNodes.Count > 2)
			throw new ArgumentException("Unexpected number of trk child nodes");

		route.Points = GetPoints(trk);

		return route;
	}

	private static (Route route, XmlNode track) GetNodeAndTrack(string filePath)
	{
		(XmlNode root, XmlNamespaceManager namespaceManager) = GetRootNode(filePath);

		XmlNode? description = root.SelectSingleNode("descendant::gpx:desc", namespaceManager);
		Route route = new() { Name = description?.InnerText ?? "No name" };

		XmlNode? track = root.SelectSingleNode("descendant::gpx:trk", namespaceManager);

		if (track is null)
			throw new ArgumentException("Could not find node trk in xml");

		return (route, track);
	}

	private static IEnumerable<Point> GetPoints(XmlNode trk)
	{
		if (trk.ChildNodes.Count > 2)
			throw new ArgumentException("Unexpected number of trk child nodes");

		XmlNode? trackSegment = trk.ChildNodes[1];
		XmlNodeList? trackParts = trackSegment?.ChildNodes;

		List<Point> points = new();

		if (trackParts is null)
			return points;

		foreach (XmlNode trackPart in trackParts)
		{
			Point point = new();
			bool skipPoint = false;
			string? lat = trackPart?.Attributes?.GetNamedItem("lat")?.InnerText;
			string? lon = trackPart?.Attributes?.GetNamedItem("lon")?.InnerText;
			if (trackPart is null || lat is null || lon is null)
				throw new ArgumentException("lat or long was null");

			point.Latitude = float.Parse(lat, System.Globalization.NumberFormatInfo.CurrentInfo);
			point.Longitude = float.Parse(lon, System.Globalization.NumberFormatInfo.CurrentInfo);

			foreach (XmlNode node in trackPart.ChildNodes)
			{
				switch (node.Name)
				{
					case "ele":
						point.Elevation = float.Parse(node.InnerText, System.Globalization.NumberFormatInfo.CurrentInfo);
						break;
					case "time":
						try
						{
							point.Time = InstantPattern.General.Parse(node.InnerText).Value;
						}
						catch (UnparsableValueException)
						{
							Console.WriteLine($"Failed to parse time {node.InnerText} because the format is invalid");
							skipPoint = true;
						}

						break;
					case "speed":
						point.Speed = float.Parse(node.InnerText, System.Globalization.NumberFormatInfo.CurrentInfo);
						break;
					case "sat":
						point.Satellites = int.Parse(node.InnerText, System.Globalization.NumberFormatInfo.CurrentInfo);
						break;
				}
			}

			if (!skipPoint)
				points.Add(point);
		}

		return points;
	}

	private static (XmlNode rootNode, XmlNamespaceManager namespaceManager) GetRootNode(string filePath)
	{
		XmlDocument doc = new();
		doc.Load(filePath);

		XmlNamespaceManager namespaceManager = new(doc.NameTable);
		namespaceManager.AddNamespace("gpx", "http://www.topografix.com/GPX/1/0");
		XmlElement? documentElement = doc.DocumentElement;
		if (documentElement is null)
			throw new ArgumentException("Doc element was null");
		return (documentElement, namespaceManager);
	}
}