﻿using Newtonsoft.Json;
using NodaTime;

namespace Routing;

public sealed class Point
{
	public float Elevation { get; set; }
	public float Latitude { get; set; }
	public float Longitude { get; set; }

	// ReSharper disable once MemberCanBePrivate.Global -- Required for JSON
	public long TimeLong { get; set; }
	public float Speed { get; set; }
	public int Satellites { get; set; }

	[JsonIgnore]
	public Instant Time { get => Instant.FromUnixTimeTicks(TimeLong); set => TimeLong = value.ToUnixTimeTicks(); }
}