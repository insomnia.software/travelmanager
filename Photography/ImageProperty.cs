﻿namespace Photography;

public enum ImageProperty
{
	None,
	CameraMake = 271,
	Orientation = 274,
	DateTaken = 36867
}