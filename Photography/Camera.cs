﻿namespace Photography;

public enum Camera
{
	Unknown,
	Gopro,
	Panasonic
}