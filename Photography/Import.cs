﻿using System.Diagnostics;
using Newtonsoft.Json;
using Directory = System.IO.Directory;

#nullable enable

namespace Photography;

public static class Import
{
	public static IReadOnlyCollection<Photo> LoadAllPhotos(string jsonFilePath, string? photosFilePath = null)
	{
		IReadOnlyCollection<Photo> photos;

		Stopwatch sw = new();
		sw.Start();
		if (File.Exists(jsonFilePath))
		{
			//Console.WriteLine($"Photo file found at {photosPath}");
			photos = JsonConvert.DeserializeObject<List<Photo>>(File.ReadAllText(jsonFilePath)) ?? new List<Photo>();
			//Console.WriteLine($"Loaded {photos.Count} routes from json in {sw.ElapsedMilliseconds}ms");
		}
		else if (photosFilePath is not null)
		{
			Console.WriteLine($"Did not find photo file found at {jsonFilePath}. Generating it...");
			photos = GetAllPhotos(photosFilePath);
			Console.WriteLine($"Loaded {photos.Count} photos in {sw.ElapsedMilliseconds}ms");

			File.WriteAllText(jsonFilePath, JsonConvert.SerializeObject(photos));
		}
		else
		{
			Console.WriteLine($"Failed to find json file at {jsonFilePath} and no photo path was supplied");
			return new List<Photo>();
		}

		return photos;
	}

	private static IReadOnlyCollection<Photo> GetAllPhotos(string parentFolder)
	{
		List<Photo> photos = new();

		foreach (string file in Directory.EnumerateFiles(parentFolder, "*.jpg", SearchOption.AllDirectories))
		{
			ImageAttributes imageAttributes = Metadata.GetImageAttributes(file);

			if (imageAttributes.TimeTakenUtcAdjusted is null)
				continue;

			photos.Add(
				new Photo
				{
					Aperture = imageAttributes.Aperture,
					CameraUsed = imageAttributes.CameraUsed,
					TimeTakenUtcAdjusted = imageAttributes.TimeTakenUtcAdjusted.Value,
					ExposureTime = imageAttributes.ExposureTime,
					FocalLength = imageAttributes.FocalLength,
					FocalLength35 = imageAttributes.FocalLength35,
					Iso = imageAttributes.Iso,
					Name = Path.GetFileNameWithoutExtension(file),
					Orientation = imageAttributes.Orientation,
					Path = file
				});
		}

		return photos;
	}
}