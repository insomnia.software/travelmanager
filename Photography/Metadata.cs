﻿using MetadataExtractor;
using NodaTime;
using NodaTime.Text;

namespace Photography;

internal static class Metadata
{
	private static string GetCameraUsed(IReadOnlyList<Tag> cameraTags)
	{
		string make = cameraTags.FirstOrDefault(x => x.Name == "Make")?.Description ?? "Unknown make";
		string model = cameraTags.FirstOrDefault(x => x.Name == "Model")?.Description ?? "Unknown model";

		return $"{make} {model}";
	}

	private static Orientation GetOrientation(IEnumerable<Tag> cameraTags)
	{
		string? orientation = cameraTags.FirstOrDefault(x => x.Name == "Orientation")?.Description;

		if (orientation is null)
			return Orientation.Unknown;

		if (orientation.Contains("normal", StringComparison.Ordinal))
			return Orientation.Landscape;
		if (orientation.Contains("Rotate 90", StringComparison.Ordinal))
			return Orientation.Portrait90;
		if (orientation.Contains("Rotate 180", StringComparison.Ordinal))
			return Orientation.Landscape180;
		if (orientation.Contains("Rotate 270", StringComparison.Ordinal))
			return Orientation.Portrait270;

		return Orientation.Unknown;
	}

	private static string GetAperture(IEnumerable<Tag> photoTags) => GetPhotoAttribute(photoTags, "F-Number");
	private static string GetExposure(IEnumerable<Tag> photoTags) => GetPhotoAttribute(photoTags, "Exposure Time");
	private static string GetFocalLength(IEnumerable<Tag> photoTags) => GetPhotoAttribute(photoTags, "Focal Length");
	private static string GetFocalLength35(IEnumerable<Tag> photoTags) => GetPhotoAttribute(photoTags, "Focal Length 35");
	private static string GetIso(IEnumerable<Tag> photoTags) => GetPhotoAttribute(photoTags, "ISO Speed Ratings");
	private static string GetPhotoAttribute(IEnumerable<Tag> photoTags, string attribute) => photoTags.FirstOrDefault(x => x.Name == attribute)?.Description ?? "Unknown";

	private static LocalDateTime? GetDateTime(IEnumerable<Tag> photoTags)
	{
		string isoDate = GetPhotoAttribute(photoTags, "Date/Time Original");
		LocalDateTimePattern pattern = LocalDateTimePattern.CreateWithInvariantCulture("yyyy:MM:dd HH:mm:ss");
		ParseResult<LocalDateTime> parseResult = pattern.Parse(isoDate);
		if (!parseResult.Success)
			return null;

		return parseResult.Value;
	}

	internal static ImageAttributes GetImageAttributes(string imagePath)
	{
		IReadOnlyCollection<MetadataExtractor.Directory> directories = ImageMetadataReader.ReadMetadata(imagePath);
		IReadOnlyList<Tag> cameraTags = directories.First(x => x.Name == "Exif IFD0").Tags;
		IReadOnlyList<Tag> photoTags = directories.First(x => x.Name == "Exif SubIFD").Tags;

		ImageAttributes imageAttributes = new()
		{
			Aperture = GetAperture(photoTags),
			CameraUsed = GetCameraUsed(cameraTags),
			TimeTakenUtcAdjusted = GetDateTime(photoTags),
			ExposureTime = GetExposure(photoTags),
			FocalLength = GetFocalLength(photoTags),
			FocalLength35 = GetFocalLength35(photoTags),
			Iso = GetIso(photoTags),
			Orientation = GetOrientation(cameraTags)
		};

		return imageAttributes;
	}
}