﻿namespace Photography;

public enum Orientation
{
	Unknown,
	Landscape,
	Landscape180,
	Portrait90,
	Portrait270
}