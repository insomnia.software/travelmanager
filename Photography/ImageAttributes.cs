﻿using NodaTime;

namespace Photography;

internal sealed record ImageAttributes
{
	public string Aperture { get; init; } = "Unknown";
	public string CameraUsed { get; init; } = "Unknown";
	public LocalDateTime? TimeTakenUtcAdjusted { get; init; }
	public string ExposureTime { get; init; } = "Unknown";
	public string FocalLength { get; init; } = "Unknown";
	public string FocalLength35 { get; init; } = "Unknown";
	public string Iso { get; init; } = "Unknown";
	public Orientation Orientation { get; set; }
}