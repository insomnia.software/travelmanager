﻿using Newtonsoft.Json;
using NodaTime;

namespace Photography;

public sealed record Photo
{
	public string Aperture { get; set; } = "Unknown";
	public string CameraUsed { get; set; } = "Unknown";

	// ReSharper disable once MemberCanBePrivate.Global -- Needed for JSON deserialisation
	public DateTime DateTimeTimeTaken { get; set; }
	public string ExposureTime { get; set; } = "Unknown";
	public string FocalLength35 { get; set; } = "Unknown";
	public string FocalLength { get; set; } = "Unknown";
	public string Iso { get; set; } = "Unknown";
	public required string Name { get; init; }
	public Orientation Orientation { get; set; }
	public required string Path { get; set; }

	[JsonIgnore]
	public required LocalDateTime TimeTakenUtcAdjusted { get => LocalDateTime.FromDateTime(DateTimeTimeTaken).Minus(Period.FromMinutes(3)).Minus(Period.FromHours(1)); init => DateTimeTimeTaken = value.ToDateTimeUnspecified(); }
}