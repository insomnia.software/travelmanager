using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using NodaTime;
using Photography;

namespace Tests;

[SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores")]
public sealed class Photos
{
	[Fact]
	public void LoadAllPhotos_LoadsPhoto()
	{
		string jsonPath = $@"C:\home\Temp\Testing\{Guid.NewGuid()}.json";

		IReadOnlyCollection<Photo> photos = Import.LoadAllPhotos(jsonPath, @"C:\home\Temp\Testing\Panasonic");
		File.Delete(jsonPath);

		photos.Should().ContainSingle();
	}

	[Fact]
	public void LoadAllPhotos_LoadsPhotoFromJson()
	{
		string jsonPath = $@"C:\home\Temp\Testing\{Guid.NewGuid()}.json";
		IReadOnlyCollection<Photo> _ = Import.LoadAllPhotos(jsonPath, @"C:\home\Temp\Testing\Panasonic");

		IReadOnlyCollection<Photo> photos = Import.LoadAllPhotos(jsonPath);
		File.Delete(jsonPath);

		photos.Should().ContainSingle();
	}

	[Fact]
	public void LoadAllPhotos_LoadsPanasonicPhoto()
	{
		Photo photo = Helpers.GetPhotoByFolderName("Panasonic");

		photo.Aperture.Should().Be("f/2.8");
		photo.CameraUsed.Should().Be("Panasonic DMC-LX15");
		photo.ExposureTime.Should().Be("1/800 sec");
		photo.FocalLength.Should().Be("11.1 mm");
		photo.FocalLength35.Should().Be("30 mm");
		photo.Iso.Should().Be("125");
		photo.Name.Should().Be("P1040383");
		photo.Orientation.Should().Be(Orientation.Portrait90);
		photo.TimeTakenUtcAdjusted.Should().BeEquivalentTo(new LocalDateTime(2022, 09, 26, 15, 41, 45));
	}

	[Fact]
	public void LoadAllPhotos_LoadsGoproPhoto()
	{
		Photo photo = Helpers.GetPhotoByFolderName("Gopro");

		photo.Aperture.Should().Be("f/2.8");
		photo.CameraUsed.Should().Be("GoPro HERO7 Black");
		photo.ExposureTime.Should().Be("1/30 sec");
		photo.FocalLength.Should().Be("3 mm");
		photo.FocalLength35.Should().Be("15 mm");
		photo.Iso.Should().Be("2777");
		photo.Name.Should().Be("G0016024");
		photo.Orientation.Should().Be(Orientation.Landscape);
		photo.TimeTakenUtcAdjusted.Should().BeEquivalentTo(new LocalDateTime(2016, 01, 14, 04, 45, 47));
	}

	[Fact]
	public void LoadAllPhotos_Load270Photo()
	{
		Photo photo = Helpers.GetPhotoByFolderName("270");

		photo.Orientation.Should().Be(Orientation.Portrait270);
	}

	[Fact]
	public void LoadAllPhotos_Load180Photo()
	{
		Photo photo = Helpers.GetPhotoByFolderName("180");

		photo.Orientation.Should().Be(Orientation.Landscape180);
	}
}

internal static class Helpers
{
	internal static Photo GetPhotoByFolderName(string folderName)
	{
		string jsonPath = $@"C:\home\Temp\Testing\{Guid.NewGuid()}.json";

		IReadOnlyCollection<Photo> photos = Import.LoadAllPhotos(jsonPath, $@"C:\home\Temp\Testing\{folderName}");
		File.Delete(jsonPath);

		return photos.First();
	}
}