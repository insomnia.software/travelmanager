﻿using NodaTime;
using Photography;
using Routing;
using Search;
using Import = Routing.Import;

#pragma warning disable CA1303

namespace ConsoleSearch;

internal class Program
{
	public static void Main()
	{
		// ReSharper disable once StringLiteralTypo
		IReadOnlyCollection<Route> routes = Import.LoadAllRoutes(@"C:\home\Temp\routes.json", @"\\192.168.1.3\tier1\Video\EuropeGPS");
		IReadOnlyCollection<Photo> photos = Photography.Import.LoadAllPhotos("C:\\home\\Temp\\photos.json", @"\\192.168.1.3\tier1\Video\Mine\Europe");

		while (true)
		{
			Console.WriteLine("Choose option:");
			Console.WriteLine("Print information about image: 1");
			Console.WriteLine("Focal length csv: 2");
			Console.WriteLine("Output subset of photos: 3");
			Console.WriteLine("Output photos from country: 4");
			Console.WriteLine("Output photos by orientation: 5");

			string? choice = Console.ReadLine();

			switch (choice)
			{
				case "1":
					Console.WriteLine("Enter image ID:");
					FindImageByName(Console.ReadLine() ?? "", photos, routes);
					break;
				case "2":
					ProduceFocalDistanceGraph(photos);
					break;
				case "3":
					OutputSubsetOfPhotosByCountry(PhotoLists.BestPhotos(), @"C:\home\Temp\Test", false, photos, routes);
					break;
				case "4":
					Console.WriteLine("Enter country name:");
					OutputAllPhotosFromCountry(@"C:\home\Temp\Country", Console.ReadLine() ?? "", photos, routes);
					break;
				case "5":
					OutputSubsetOfPhotosByOrientation(PhotoLists.BestPhotos(), @"C:\home\Temp\Test", photos);
					break;
				case "q":
					return;
			}
		}
	}

	private static void ProduceFocalDistanceGraph(IEnumerable<Photo> photos)
	{
		SortedDictionary<string, int> focalLengths = new();
		foreach (Photo photo in photos.Where(x => x.CameraUsed.Contains("Panasonic", StringComparison.Ordinal)))
		{
			if (focalLengths.ContainsKey(photo.FocalLength35))
				focalLengths[photo.FocalLength35] += 1;
			else
				focalLengths.Add(photo.FocalLength35, 1);
		}

		foreach (KeyValuePair<string, int> kvp in focalLengths)
			Console.WriteLine($"{kvp.Key}\t{kvp.Value}");
	}

	private static void FindImageByName(string name, IEnumerable<Photo> photos, IEnumerable<Route> routes)
	{
		SearchResult? searchResult = ByPhoto.WhereWasAPhotoTaken(name, photos, routes);

		if (searchResult is null)
		{
			Console.WriteLine($"Could not find result for {name}");
			return;
		}

		PrintResult(searchResult.Photo, searchResult.Location, searchResult.Country);
	}

	private static void PrintResult(Photo photo, Point location, Country country)
	{
		Console.WriteLine($"Name: {photo.Name}");
		Console.WriteLine($"Country: {country.Name}");
		Console.WriteLine($"Camera time UTC: {photo.TimeTakenUtcAdjusted.ToString()}");
		Console.WriteLine($"Camera local time: {TimeInCountry(photo.TimeTakenUtcAdjusted, country).ToString()}");
		Console.WriteLine($"Latitude: {location.Latitude}");
		Console.WriteLine($"Longitude: {location.Longitude}");
		Console.WriteLine($"Time difference: {(photo.TimeTakenUtcAdjusted.ToUtcInstant() - location.Time).Abs().ToString()}");
		Console.WriteLine($"Map overview: https://www.google.com/maps/place/{location.Latitude},+{location.Longitude}/@{location.Latitude},{location.Longitude},7z");
		Console.WriteLine($"Map tight: https://www.google.com/maps/place/{location.Latitude},+{location.Longitude}/@{location.Latitude},{location.Longitude},18z");
		Console.WriteLine($"File path: {photo.Path}");
	}

	private static LocalDateTime TimeInCountry(LocalDateTime time, Country country) => time.PlusHours(country.UtcOffset);

	private static void OutputAllPhotosFromCountry(string destinationFolder, string country, IReadOnlyCollection<Photo> allPhotos, IReadOnlyCollection<Route> routes)
	{
		foreach (string photoString in allPhotos.Select(x => x.Name))
		{
			SearchResult? searchResult = ByPhoto.WhereWasAPhotoTaken(photoString, allPhotos, routes);

			if (searchResult is null)
			{
				Console.WriteLine($"Failed to find photo {photoString}");
				continue;
			}

			if (searchResult.Country.Name != country)
				continue;

			if (searchResult.Photo.Path.Contains("Best", StringComparison.Ordinal) || searchResult.Photo.Path.Contains("Gopro", StringComparison.Ordinal))
				continue;

			string savePath = Path.Combine(destinationFolder, country);
			string photoPath = searchResult.Photo.Path;
			string destinationPath = Path.Combine(savePath, Path.GetFileName(photoPath));
			Directory.CreateDirectory(savePath);
			File.Copy(photoPath, destinationPath);
		}
	}

	private static void OutputSubsetOfPhotosByCountry(IReadOnlyCollection<string> selectedPhotos, string destinationFolder, bool includeRaw, IReadOnlyCollection<Photo> allPhotos, IReadOnlyCollection<Route> routes)
	{
		int count2 = 0;
		foreach (string photoString in selectedPhotos)
		{
			count2++;
			SearchResult? searchResult = ByPhoto.WhereWasAPhotoTaken(photoString, allPhotos, routes);

			if (searchResult is null)
			{
				Console.WriteLine($"Failed to find photo {photoString}");
				continue;
			}

			if (searchResult.Photo.Path.Contains("Best", StringComparison.Ordinal) || searchResult.Photo.Path.Contains("Gopro", StringComparison.Ordinal))
				continue;

			string savePath = Path.Combine(destinationFolder, searchResult.Country.Name);
			string photoPath = searchResult.Photo.Path;
			string photoPathRaw = Path.ChangeExtension(photoPath, "RW2");
			string destinationPath = Path.Combine(savePath, Path.GetFileName(photoPath));
			string destinationPathRaw = Path.Combine(savePath, Path.GetFileName(photoPathRaw));

			Directory.CreateDirectory(savePath);
			Console.WriteLine($"Copying file {count2} of {selectedPhotos.Count}");
			try
			{
				Console.WriteLine($"Copying {photoPath} to {destinationPath}");
				File.Copy(photoPath, destinationPath);
			}
			catch (Exception ex) when (ex is UnauthorizedAccessException or ArgumentException or PathTooLongException or DirectoryNotFoundException or FileNotFoundException or NotSupportedException or IOException)
			{
				Console.WriteLine($"Error copying from {photoPath} to {destinationPath}");
				Console.WriteLine(ex);
			}

			try
			{
				if (includeRaw)
					File.Copy(photoPathRaw, destinationPathRaw);
			}
			catch (Exception ex) when (ex is UnauthorizedAccessException or ArgumentException or PathTooLongException or DirectoryNotFoundException or FileNotFoundException or NotSupportedException or IOException)
			{
				Console.WriteLine($"Error copying from {photoPathRaw} to {destinationPathRaw}");
				Console.WriteLine(ex);
			}
		}

		Console.WriteLine(count2);
	}

	private static void OutputSubsetOfPhotosByOrientation(IReadOnlyCollection<string> selectedPhotos, string destinationFolder, IReadOnlyCollection<Photo> allPhotos)
	{
		int count = 0;
		foreach (string photoString in selectedPhotos)
		{
			count++;
			Photo photo = allPhotos.First(x => x.Name.Contains(photoString, StringComparison.Ordinal));

			string orientationString;
			switch (photo.Orientation)
			{
				case Orientation.Landscape or Orientation.Landscape180:
					orientationString = "Landscape";
					break;
				case Orientation.Portrait90 or Orientation.Portrait270:
					orientationString = "Portrait";
					break;
				default:
					Console.WriteLine($"Could not find orientation of photo {photoString}");
					continue;
			}

			string savePath = Path.Combine(destinationFolder, orientationString);
			string photoPath = photo.Path;
			string destinationPath = Path.Combine(savePath, Path.GetFileName(photoPath));

			Directory.CreateDirectory(savePath);
			Console.WriteLine($"Copying file {count} of {selectedPhotos.Count}");
			try
			{
				Console.WriteLine($"Copying {photoPath} to {destinationPath}");
				File.Copy(photoPath, destinationPath);
			}
			catch (Exception ex) when (ex is UnauthorizedAccessException or ArgumentException or PathTooLongException or DirectoryNotFoundException or FileNotFoundException or NotSupportedException or IOException)
			{
				Console.WriteLine($"Error copying from {photoPath} to {destinationPath}");
				Console.WriteLine(ex);
			}
		}

		Console.WriteLine(count);
	}
}