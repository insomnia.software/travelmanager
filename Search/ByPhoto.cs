﻿using NodaTime;
using Photography;
using Routing;

namespace Search;

public static class ByPhoto
{
	public static SearchResult? WhereWasAPhotoTaken(string photoName, IEnumerable<Photo> photos, IEnumerable<Route> routes)
	{
		// ReSharper disable ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
		if (photos is null || routes is null)
			return null;
		// ReSharper enable ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract

		Photo? foundPhoto = GetPhotoByName(photoName, photos);

		if (foundPhoto is null)
			return null;

		(Point point, Duration difference) = GetClosestPointToTime(routes, foundPhoto.TimeTakenUtcAdjusted.ToUtcInstant());

		if (difference > Duration.FromHours(1.5))
			return new SearchResult { Country = new Country { Name = "Unknown" }, Photo = foundPhoto, Location = point };

		return new SearchResult { Country = GetCountryFromPhoto(foundPhoto), Photo = foundPhoto, Location = point };
	}

	private static Photo? GetPhotoByName(string photoName, IEnumerable<Photo> photos)
	{
		Photo? foundPhoto = null;
		foreach (Photo photo in photos)
		{
			if (photo.Name.Contains(photoName, StringComparison.CurrentCulture))
				foundPhoto = photo;
		}

		return foundPhoto;
	}

	private static (Point point, Duration difference) GetClosestPointToTime(IEnumerable<Route> routes, Instant time)
	{
		Point currentClosestPoint = new();
		Duration currentClosestTime = Duration.MaxValue;

		foreach (Route route in routes)
		{
			foreach (Point point in route.Points)
			{
				Duration difference = point.Time - time;
				difference = Duration.FromMilliseconds(Math.Abs(difference.TotalMilliseconds));

				if (difference < currentClosestTime)
				{
					currentClosestTime = difference;
					currentClosestPoint = point;
				}
			}
		}

		return (currentClosestPoint, currentClosestTime);
	}

	private static Country GetCountryFromPhoto(Photo photo)
	{
		foreach (Country country in CountriesVisited())
		{
			Instant timeTaken = photo.TimeTakenUtcAdjusted.ToUtcInstant();
			if (timeTaken > country.Entry && timeTaken < country.Exit)
				return country;
		}

		Console.WriteLine($"Could not find country for photo {photo.Name}. Time taken was {photo.TimeTakenUtcAdjusted} which is instant {photo.TimeTakenUtcAdjusted.ToUtcInstant()}");
		return new Country();
	}

	private static Instant InstantFromDate(int day, int hour, int minute, int second) => Instant.FromDateTimeUtc(new DateTime(2022, 9, day, hour, minute, second, DateTimeKind.Utc));

	private static Instant InstantFromDateOct(int day, int hour, int minute, int second) => Instant.FromDateTimeUtc(new DateTime(2022, 10, day, hour, minute, second, DateTimeKind.Utc));

	private static IReadOnlyCollection<Country> CountriesVisited()
	{
		List<Country> countries = new()
		{
			new Country { Name = "UK", Entry = InstantFromDate(26, 1, 0, 0), Exit = InstantFromDate(26, 17, 36, 28), UtcOffset = 1 },
			new Country { Name = "France", Entry = InstantFromDate(26, 17, 21, 0), Exit = InstantFromDate(26, 18, 40, 48), UtcOffset = 2 },
			new Country { Name = "Belgium", Entry = InstantFromDate(26, 18, 40, 41), Exit = InstantFromDate(27, 13, 33, 33), UtcOffset = 2 },
			new Country { Name = "Germany", Entry = InstantFromDate(27, 13, 33, 34), Exit = InstantFromDate(29, 07, 24, 22), UtcOffset = 2 },
			new Country { Name = "Czech", Entry = InstantFromDate(29, 07, 24, 23), Exit = InstantFromDate(30, 06, 17, 58), UtcOffset = 2 },
			new Country { Name = "Poland", Entry = InstantFromDate(30, 06, 17, 59), Exit = InstantFromDateOct(1, 11, 53, 14), UtcOffset = 2 },
			new Country { Name = "Slovakia", Entry = InstantFromDateOct(1, 11, 53, 15), Exit = InstantFromDateOct(1, 15, 43, 32), UtcOffset = 2 },
			new Country { Name = "Hungary", Entry = InstantFromDateOct(1, 15, 43, 33), Exit = InstantFromDateOct(2, 7, 53, 58), UtcOffset = 2 },
			new Country { Name = "Romania", Entry = InstantFromDateOct(2, 7, 53, 59), Exit = InstantFromDateOct(4, 6, 8, 51), UtcOffset = 3 },
			new Country { Name = "Bulgaria", Entry = InstantFromDateOct(4, 6, 8, 52), Exit = InstantFromDateOct(5, 8, 25, 20), UtcOffset = 3 },
			new Country { Name = "Turkey", Entry = InstantFromDateOct(5, 8, 25, 21), Exit = InstantFromDateOct(18, 11, 18, 59), UtcOffset = 3 },
			new Country { Name = "Greece", Entry = InstantFromDateOct(18, 11, 19, 00), Exit = InstantFromDateOct(19, 13, 9, 53), UtcOffset = 3 },
			new Country { Name = "Albania", Entry = InstantFromDateOct(19, 13, 9, 54), Exit = InstantFromDateOct(20, 16, 38, 50), UtcOffset = 2 },
			new Country { Name = "Kosovo", Entry = InstantFromDateOct(20, 16, 38, 51), Exit = InstantFromDateOct(21, 10, 26, 08), UtcOffset = 2 },
			new Country { Name = "Montenegro", Entry = InstantFromDateOct(21, 10, 26, 09), Exit = InstantFromDateOct(22, 12, 8, 57), UtcOffset = 2 },
			new Country { Name = "Bosnia", Entry = InstantFromDateOct(22, 12, 8, 58), Exit = InstantFromDateOct(23, 6, 9, 57), UtcOffset = 2 },
			new Country { Name = "Croatia", Entry = InstantFromDateOct(23, 6, 9, 58), Exit = InstantFromDateOct(24, 6, 9, 45), UtcOffset = 2 },
			new Country { Name = "Slovenia", Entry = InstantFromDateOct(24, 6, 9, 46), Exit = InstantFromDateOct(24, 15, 10, 59), UtcOffset = 2 },
			new Country { Name = "Austria", Entry = InstantFromDateOct(24, 15, 17, 08), Exit = InstantFromDateOct(26, 6, 2, 29), UtcOffset = 2 },
			new Country { Name = "Liechtenstein", Entry = InstantFromDateOct(26, 6, 2, 30), Exit = InstantFromDateOct(26, 6, 15, 37), UtcOffset = 2 },
			new Country { Name = "Switzerland", Entry = InstantFromDateOct(26, 6, 15, 38), Exit = InstantFromDateOct(26, 6, 43, 5), UtcOffset = 2 },
			new Country { Name = "Liechtenstein", Entry = InstantFromDateOct(26, 6, 43, 6), Exit = InstantFromDateOct(26, 8, 50, 18), UtcOffset = 2 },
			new Country { Name = "Switzerland", Entry = InstantFromDateOct(26, 8, 50, 18), Exit = InstantFromDateOct(27, 15, 1, 59), UtcOffset = 2 },
			new Country { Name = "France", Entry = InstantFromDateOct(27, 15, 2, 0), Exit = InstantFromDateOct(28, 8, 1, 19), UtcOffset = 2 },
			new Country { Name = "Luxembourg", Entry = InstantFromDateOct(28, 8, 1, 20), Exit = InstantFromDateOct(28, 11, 44, 09), UtcOffset = 2 },
			new Country { Name = "Germany", Entry = InstantFromDateOct(28, 11, 44, 10), Exit = InstantFromDateOct(28, 17, 11, 17), UtcOffset = 2 },
			new Country { Name = "Netherlands", Entry = InstantFromDateOct(28, 17, 11, 18), Exit = InstantFromDateOct(28, 18, 54, 25), UtcOffset = 2 },
			new Country { Name = "France", Entry = InstantFromDateOct(28, 18, 54, 26), Exit = InstantFromDateOct(29, 9, 36, 0), UtcOffset = 2 },
			new Country { Name = "UK", Entry = InstantFromDateOct(29, 9, 36, 1), Exit = InstantFromDateOct(30, 1, 0, 0), UtcOffset = 1 }
		};

		return countries;
	}
}