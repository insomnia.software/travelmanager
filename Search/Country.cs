﻿using NodaTime;

namespace Search;

public sealed class Country
{
	public string Name { get; set; }
	public Instant Entry { get; set; }
	public Instant Exit { get; set; }
	public int UtcOffset { get; set; }
}