﻿using Routing;

namespace Search;

// ReSharper disable once UnusedType.Global
public static class ByPoint
{
	// ReSharper disable once UnusedMember.Global
	public static void PrintClosestPoint(IEnumerable<Route> routes, double lat, double lon)
	{
		// ReSharper disable once ConditionIsAlwaysTrueOrFalseAccordingToNullableAPIContract
		if (routes is null)
			return;

		Point closestPoint = GetClosestPointToRoutes(routes, lat, lon);
		double meters = MetersBetweenTwoPoints(lat, lon, closestPoint.Latitude, closestPoint.Longitude);
		Console.WriteLine($"Found a {closestPoint.Latitude}, {closestPoint.Longitude} that was {Math.Round(meters)} meters away at time {closestPoint.Time}");
	}

	private static double MetersBetweenTwoPoints(double lat1, double lon1, double lat2, double lon2)
	{
		const double R = 6371e3; // metres
		double φ1 = lat1 * Math.PI / 180;
		double φ2 = lat2 * Math.PI / 180;
		double δφ = (lat2 - lat1) * Math.PI / 180;
		double δλ = (lon2 - lon1) * Math.PI / 180;

		double a = Math.Sin(δφ / 2) * Math.Sin(δφ / 2) +
				   Math.Cos(φ1) * Math.Cos(φ2) *
				   Math.Sin(δλ / 2) * Math.Sin(δλ / 2);
		double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

		double d = R * c;
		return d;
	}

	/// <summary>Use this to work out what time I was at a given point. Using coordinates from the map, this will return the closest point and the time I was there</summary>
	private static Point GetClosestPointToRoutes(IEnumerable<Route> routes, double lat, double lon)
	{
		Point currentClosest = new();
		double currentClosestDistance = double.MaxValue;

		foreach (Route route in routes)
		{
			(Point point, double distance) = GetClosestPointToRoute(route, lat, lon);
			if (distance < currentClosestDistance)
			{
				currentClosestDistance = distance;
				currentClosest = point;
			}
		}

		return currentClosest;
	}

	private static (Point point, double distance) GetClosestPointToRoute(Route route, double lat, double lon)
	{
		Point currentClosest = new();
		double currentClosestDistance = double.MaxValue;

		foreach (Point point in route.Points)
		{
			double distance = Math.Abs(lat - point.Latitude) + Math.Abs(lon - point.Longitude);

			if (distance < currentClosestDistance)
			{
				currentClosestDistance = distance;
				currentClosest = point;
			}
		}

		return (currentClosest, currentClosestDistance);
	}
}