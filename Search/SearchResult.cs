﻿using Photography;
using Routing;

namespace Search;

public sealed class SearchResult
{
	public Photo Photo { get; set; }
	public Country Country { get; set; }
	public Point Location { get; set; }
}