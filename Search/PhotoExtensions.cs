﻿using NodaTime;

namespace Search;

public static class PhotoExtensions
{
	public static Instant ToUtcInstant(this LocalDateTime localDateTime) => localDateTime.InZoneStrictly(DateTimeZone.Utc).ToInstant();

	public static Duration Abs(this Duration duration) => Duration.FromMilliseconds(Math.Abs(duration.TotalMilliseconds));
}